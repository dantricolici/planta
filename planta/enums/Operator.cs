﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace planta.enums
{
    public enum Operator
    {
        EGAL,
        MAI_MIC,
        MAI_MARE,
        MAI_MIC_EGAL,
        MAI_MARE_EGAL
    }
}
