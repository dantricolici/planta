﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace planta.enums
{
    public enum FunctionResult
    {
        FUNCTION_WASNT_START,
        TIME_TO_MULTIPLICATE,
        LEAVES_WAS_UPGRADE,
        ROOTS_WAS_UPGRADE,
        HIEGHT_WAS_UPGRADE
    }
}
