﻿using planta.model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace planta.settings
{
    public class GameSetings
    {
        public const int StartSolarEnergy = 10;
        public const int StartWaterLevel = 10;

        private static readonly UpgradeCost HieghtCost = new UpgradeCost(0.5, 0.4);
        private static readonly UpgradeCost RootsCost = new UpgradeCost(0.5, 0.1);
        private static readonly UpgradeCost leavesCost = new UpgradeCost(0.1, 0.3);

        public static void UpgradeHieght(Plant plant)
        {
            if (plant.SolarEnergy - HieghtCost.SolarEnergyCost <= 0 || plant.WaterLevel - HieghtCost.WaterLevelCost <= 0)
                return;
            plant.SolarEnergy -= HieghtCost.SolarEnergyCost;
            plant.WaterLevel -= HieghtCost.WaterLevelCost;
            plant.Height += 0.5;
        }

        public static void UpgradeLeaves(Plant plant)
        {
            if (plant.SolarEnergy - leavesCost.SolarEnergyCost <= 0 || plant.WaterLevel - leavesCost.WaterLevelCost <= 0)
                return;
            plant.SolarEnergy -= leavesCost.SolarEnergyCost;
            plant.WaterLevel -= leavesCost.WaterLevelCost;
            plant.Leaves += 0.5;
        }

        public static void UpgradeRoots(Plant plant)
        {
            if (plant.SolarEnergy - RootsCost.SolarEnergyCost <= 0 || plant.WaterLevel - RootsCost.WaterLevelCost <= 0)
                return;
            plant.SolarEnergy -= RootsCost.SolarEnergyCost;
            plant.WaterLevel -= RootsCost.WaterLevelCost;
            plant.Roots += 0.5;
        }
    }
}
