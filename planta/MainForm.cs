﻿using planta.model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace planta
{
    public partial class mainForm : Form
    {
        private Harta harta = null;
        private PlantWrappers plantWrapper = null;

        public mainForm()
        {
            InitializeComponent();
        }

        private void buttonGenerate_Click(object sender, EventArgs e)
        {
            harta = new Harta(10, 10);
            plantWrapper = new PlantWrappers(ref harta);
            pictureBox.Invalidate();
        }

        private void pictureBox_Paint(object sender, PaintEventArgs e)
        {
            if (harta == null) return;

            int cellW = pictureBox.Width / harta.Width;
            int cellH = pictureBox.Height / harta.Height;

            bool[,] IsPlantHere = new bool[harta.Width, harta.Height];

            for (int i = 0; i < harta.PLants.Count; i++)
            {
                IsPlantHere[harta.PLants[i].Coordinats.X, harta.PLants[i].Coordinats.Y] = true;
            }

            for (int x = 0; x < harta.Width; x++)
            {
                for (int y = 0; y < harta.Height; y++)
                {
                    e.Graphics.FillRectangle(harta.Map[x, y].Brush, cellW * x, cellH * y, cellW + 10, cellH + 10);
                    if(IsPlantHere[x, y])
                    {
                        e.Graphics.FillEllipse(Brushes.Green, x * cellW - 1, y * cellH - 1, cellW, cellH);
                        e.Graphics.DrawEllipse(new Pen(Color.Black, 5), x * cellW - 2, y * cellH - 2, cellW - 1, cellH - 1);
                    }
                }
            }
        }

        private void mainForm_Resize(object sender, EventArgs e)
        {
            pictureBox.Invalidate();
        }

        private void NextMoveButton_Click(object sender, EventArgs e)
        {
            plantWrapper.NextMove();
        }
    }
}
