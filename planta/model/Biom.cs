﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace planta.model
{
    public class Biom
    {
        public string Name { get; private set; }
        public Color Color { get; private set; }

        public Brush Brush { get; private set; }
        public double Sansa { get; private set; }

        public Biom(string name, Color color, double sansa)
        {
            Name = name;
            Color = color;
            Brush = new SolidBrush(color);
            Sansa = sansa;
        }

        private static readonly List<Biom> allBioms = new List<Biom>()
        {
            new Biom("water", Color.FromArgb(100, 100, 255), 50) ,
            new Biom("summer", Color.FromArgb(125, 255, 125), 20),
            new Biom("winter", Color.FromArgb(230,230,230), 15),
            new Biom("desert", Color.FromArgb(255, 255, 100), 15)
        };

        public static List<Biom> getAllBioms()
        {
            return allBioms;
        }
    }
}
