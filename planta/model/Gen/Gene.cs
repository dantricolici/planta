﻿using planta.enums;
using planta.settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace planta.model.Gen
{
    public class Gene
    {
        public bool itsPublic;
        public GeneTrigger geneTrigger;
        public Plant plant;

        public int HieghtUpgradeCount;
        public int LeavesUpgradeCount;
        public int RootsUpgradeCount;
        public bool TimeToMultiplicate;

        public Gene(Plant plant)
        {
            this.plant = plant;
        }

        public FunctionResult StartGen()
        {
            if (UpgradeFor(HieghtUpgradeCount, PlantProperty.HIEGHT))
                return FunctionResult.HIEGHT_WAS_UPGRADE;
            if (UpgradeFor(LeavesUpgradeCount, PlantProperty.LEAVES))
                return FunctionResult.LEAVES_WAS_UPGRADE;
            if (UpgradeFor(RootsUpgradeCount, PlantProperty.ROOTS))
                return FunctionResult.ROOTS_WAS_UPGRADE;
            if (TimeToMultiplicate)
                return FunctionResult.TIME_TO_MULTIPLICATE;

            return FunctionResult.FUNCTION_WASNT_START;
        }

        private bool UpgradeFor(int max, PlantProperty pp)
        {
            if (max == 0) return false;
            for(int i = 0; i < max; i++)
            {
                switch (pp)
                {
                    case PlantProperty.HIEGHT:
                        GameSetings.UpgradeHieght(plant);
                        break;
                    case PlantProperty.LEAVES:
                        GameSetings.UpgradeLeaves(plant);
                        break;
                    case PlantProperty.ROOTS:
                        GameSetings.UpgradeRoots(plant);
                        break;
                }
            }
            return true;
        }
    }
}
