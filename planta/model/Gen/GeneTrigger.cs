﻿using planta.enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace planta.model.Gen
{
    public class GeneTrigger
    {
        public Operator OPERATOR;
        public double NumberA;
        public double NumberB;

        public GeneTrigger(Operator OPERATOR, double NumA, double NumB)
        {
            this.OPERATOR = OPERATOR;
            NumberA = NumA;
            NumberB = NumB;
        }

        public GeneTrigger(Operator OPERATOR, ref double NumA, double NumB)
        {
            this.OPERATOR = OPERATOR;
            NumberA = NumA;
            NumberB = NumB;
        }
    }
}
