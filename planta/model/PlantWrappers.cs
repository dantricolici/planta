﻿using planta.enums;
using planta.model.Gen;
using planta.settings;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace planta.model
{
    public class PlantWrappers
    {
        private Harta map;
        private Plant parent;
        private readonly Random rnd = new Random();


        public PlantWrappers(ref Harta map)
        {
            this.map = map;
        }

        public void Multiplication(int PLantIndex)
        {
            Plant child;
            parent = map.PLants[PLantIndex];
            Point childPoint = SearchPlaceForPlant();

            if (!childPoint.IsEmpty)
            {
                child = CreateChildPlant(childPoint);
                map.PLants.Add(child);
            }
        }

        private Plant CreateChildPlant(Point point)
        {
            Plant child = new Plant
            {
                SolarEnergy = parent.SolarEnergy / 2,
                WaterLevel = parent.WaterLevel / 2,
                PlantGene = parent.PlantGene,
                Coordinats = point,
                Height = 1,
                Leaves = 1,
                Roots = 1,
                Age = 0
            };

            parent.SolarEnergy /= 2.5;
            parent.WaterLevel /= 2.5;

            return child;
        }

        private Point SearchPlaceForPlant()
        {
            int x = parent.Coordinats.X;
            int y = parent.Coordinats.Y;

            List<Point> PosibleFreePoints = new List<Point>()
            {
                new Point(x - 1, y - 1),
                new Point(x - 1, y),
                new Point(x - 1, y + 1),
                new Point(x, y - 1),
                new Point(x, y + 1),
                new Point(x + 1, y - 1),
                new Point(x + 1, y),
                new Point(x + 1, y + 1)
            };

            return AlghoritmToSearchFreePoint(PosibleFreePoints);
        }

        private Point AlghoritmToSearchFreePoint(List<Point> PosibleFreePoints)
        {
            Point FreePoint = new Point();
            bool itsOver = false;
            while (itsOver == false)
            {
                if (PosibleFreePoints.Count == 0)
                    break;
                int randomNumber = rnd.Next(PosibleFreePoints.Count);
                FreePoint = PosibleFreePoints[randomNumber];
                if (FreePoint.X < 0 || FreePoint.Y < 0 || FreePoint.X > map.Width || FreePoint.Y > map.Height)
                {
                    PosibleFreePoints.RemoveAt(randomNumber);
                    continue;
                }
                if (map.Map[FreePoint.X, FreePoint.Y] != Biom.getAllBioms()[0])
                {
                    for (int i = 0; i < map.PLants.Count; i++)
                    {
                        if (map.PLants[i].Coordinats == FreePoint)
                        {
                            FreePoint = map.PLants[i].Coordinats;
                            itsOver = true;
                            break;
                        }
                    }
                }
                PosibleFreePoints.RemoveAt(randomNumber);
            }
            return FreePoint;
        }

        public void SetDefoultsGene(ref Plant plant)
        {
            plant.PlantGene = new List<Gene>
            {
                new Gene(plant)
                {
                    itsPublic = true,
                    TimeToMultiplicate = true,
                    geneTrigger = new GeneTrigger(enums.Operator.EGAL, plant.Age, 15)
                },

                new Gene(plant)
                {
                    itsPublic = true,
                    TimeToMultiplicate = false,
                    geneTrigger = new GeneTrigger(enums.Operator.MAI_MARE_EGAL, plant.SolarEnergy, 5),
                    LeavesUpgradeCount = 1
                },

                new Gene(plant)
                {
                    itsPublic = true,
                    TimeToMultiplicate = false,
                    geneTrigger = new GeneTrigger(enums.Operator.MAI_MARE_EGAL, plant.SolarEnergy, 5),
                    HieghtUpgradeCount = 1
                },

                new Gene(plant)
                {
                    itsPublic = true,
                    TimeToMultiplicate = false,
                    geneTrigger = new GeneTrigger(enums.Operator.MAI_MARE_EGAL, plant.SolarEnergy, 5),
                    RootsUpgradeCount = 1
                }
            };
        }

        public void NextMove()
        {
            for(int i = 0; i < map.PLants.Count; i++)
            {
                Plant plant = map.PLants[i];
                if(plant.SolarEnergy <= 0 || plant.WaterLevel <= 0)
                {
                    map.PLants.Remove(plant);
                    continue;
                }
                plant.Age++;
                for (int j = 0; j < plant.PlantGene.Count; j++)
                {
                    if (plant.PlantGene[j].itsPublic)
                        switch (plant.PlantGene[j].StartGen())
                        {
                            case FunctionResult.HIEGHT_WAS_UPGRADE:
                            case FunctionResult.LEAVES_WAS_UPGRADE:
                            case FunctionResult.ROOTS_WAS_UPGRADE:
                                j = plant.PlantGene.Count;
                                break;
                            case FunctionResult.FUNCTION_WASNT_START:
                                break;
                            case FunctionResult.TIME_TO_MULTIPLICATE:
                                Multiplication(i);
                                break;
                        }
                }
            }
        }
    }
}
