﻿using System;
using System.Linq;
using System.Collections.Generic;
using planta.settings;
using System.Drawing;

namespace planta.model
{
    public class MapGenerator
    {
        private static Random rnd = new Random();
        private Harta harta;
        private List<Biom> freeBioms;
        private List<Point> FreePoints;

        public MapGenerator(Harta harta)
        {
            this.harta = harta;
        }

        public void generate()
        {
            calculateBiomRates();
            fillMap();
        }

        private void calculateBiomRates()
        {
            freeBioms = new List<Biom>();

            int totalCells = harta.Width * harta.Height;

            var bioms = Biom.getAllBioms();

            for (int i=0; i<bioms.Count; i++)
            {
                double cateAvem = (bioms[i].Sansa / 100.0 * totalCells);
                int rate = (int) Math.Round(cateAvem, 0);
                addFreeBioms(bioms[i], rate);
            }
        }

        private void addFreeBioms(Biom biom, int count)
        {
            for (int i=0; i<count; i++)
            {
                freeBioms.Add(biom);
            }
        }

        private Biom getFreeBiomOfType(Biom dedorit)
        {
            for (int i=0; i<freeBioms.Count; i++)
            {
                if (freeBioms[i] == dedorit)
                {
                    var result = freeBioms[i];
                    freeBioms.RemoveAt(i);
                    return result;
                }
            }
            return null;
        }

        private Biom scoateFreeBiom(Biom deDorit)
        {
            Biom result = null;
            if (rnd.Next(100) < MC.SansaDeRaspindireBiom)
            {
                result = getFreeBiomOfType(deDorit);
                if (result != null) return result;
            }

            int idx = rnd.Next(freeBioms.Count);
            result = freeBioms[idx];
            freeBioms.RemoveAt(idx);
            return result;
        }

        private void fillMap()
        {
            UmpleFreePoints();

            while(FreePoints.Count != 0)
            {
                int index = rnd.Next(FreePoints.Count);
                Point point = FreePoints[index];
                FreePoints.RemoveAt(index);

                var deDorit = cautaBiomulDeDorit(point.X, point.Y);
                harta.Map[point.X, point.Y] = scoateFreeBiom(deDorit);
            }
            /*
            for (int i=0; i<harta.Width; i++)
            {
                for (int j=0; j<harta.Height; j++)
                {
                    var deDorit = cautaBiomulDeDorit(i, j);
                    harta.Map[i, j] = scoateFreeBiom(deDorit);
                }
            }
            */
        }

        private void UmpleFreePoints()
        {
            FreePoints = new List<Point>();

            for (int i = 0; i < harta.Width; i++)
            {
                for (int j = 0; j < harta.Height; j++)
                {
                    FreePoints.Add(new Point(i,j));
                }
            }
        }

        private Biom cautaBiomulDeDorit(int x, int y)
        {
            List<Biom> vecini = new List<Biom>();

            adaugaVecin(vecini, x - 1, y);
            adaugaVecin(vecini, x + 1, y);
            adaugaVecin(vecini, x, y - 1);
            adaugaVecin(vecini, x, y + 1);
            adaugaVecin(vecini, x - 1, y - 1);
            adaugaVecin(vecini, x - 1, y + 1);
            adaugaVecin(vecini, x + 1, y - 1);
            adaugaVecin(vecini, x + 1, y + 1);

            return arataCelMaiBurdanosVecin(vecini);
        }

        private void adaugaVecin(List<Biom> vecini, int x, int y)
        {
            if ((x>=0)&&(x<harta.Width)&&(y>=0)&&(y<harta.Height))
            {
                vecini.Add(harta.Map[x, y]);
            }
        }

        private Biom arataCelMaiBurdanosVecin(List<Biom> vecini)
        {
            var dic = new Dictionary<Biom, int>();

            for (int i=0; i<vecini.Count; i++)
            {
                Biom v = vecini[i];
                if (v == null) continue;
                if (dic.ContainsKey(v))
                {
                    dic[v] = dic[v] + 1;
                }
                else
                {
                    dic.Add(v, 1);
                }
            }

            if (dic.Count == 0)
            {
                return null;
            }

            return dic.Aggregate((a, b) => a.Value > b.Value ? a : b).Key;
        }
    }
}
