﻿using planta.model.Gen;
using planta.settings;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace planta.model
{
    public class Plant
    {
        public int Age = 0;
        public double Height { get; set; }
        public double Roots { get; set; }
        public double Leaves { get; set; }

        public Point Coordinats = new Point();
        public List<Gene> PlantGene;

        public double SolarEnergy;
        public double WaterLevel;

        public Plant()
        {
            Height = 1;
            Roots = 1;
            Leaves = 1;
            SolarEnergy = GameSetings.StartSolarEnergy;
            WaterLevel = GameSetings.StartWaterLevel;
        }

        public void UpgradePlant(PlantProperty plantProperty)
        {
            switch (plantProperty)
            {
                case PlantProperty.HIEGHT:
                    GameSetings.UpgradeHieght(this);
                    break;
                case PlantProperty.LEAVES:
                    GameSetings.UpgradeLeaves(this);
                    break;
                case PlantProperty.ROOTS:
                    GameSetings.UpgradeRoots(this);
                    break;
            }
        }
    }
}
