﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace planta.model
{
    public class UpgradeCost
    {
        public double SolarEnergyCost;
        public double WaterLevelCost;

        public UpgradeCost(double solarEnergyCost, double waterLevelCost)
        {
            SolarEnergyCost = solarEnergyCost;
            WaterLevelCost = waterLevelCost;
        }
    }
}
