﻿using System;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace planta.model
{
    public class Harta
    {
        private static Random rnd = new Random();

        public int Width { get; private set; }
        public int Height { get; private set; }

        public Biom[,] Map { get; private set; }

        public List<Plant> PLants = new List<Plant>();

        public Harta(int width, int height)
        {
            Width = width;
            Height = height;
            Map = new Biom[width, height];
            generateMap();
        }

        private void generateMap()
        {
            var generator = new MapGenerator(this);
            generator.generate();
        }
    }
}
